var gCoursesDB = {
    description: "This DB includes all courses in system",
    courses: [
        {
            id: 1,
            courseCode: "FE_WEB_ANGULAR_101",
            courseName: "How to easily create a website with Angular",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-angular.jpg",
            teacherName: "Morris Mccoy",
            teacherPhoto: "images/teacher/morris_mccoy.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 2,
            courseCode: "BE_WEB_PYTHON_301",
            courseName: "The Python Course: build web application",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-python.jpg",
            teacherName: "Claire Robertson",
            teacherPhoto: "images/teacher/claire_robertson.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 5,
            courseCode: "FE_WEB_GRAPHQL_104",
            courseName: "GraphQL: introduction to graphQL for beginners",
            price: 850,
            discountPrice: 650,
            duration: "2h 15m",
            level: "Intermediate",
            coverImage: "images/courses/course-graphql.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 6,
            courseCode: "FE_WEB_JS_210",
            courseName: "Getting Started with JavaScript",
            price: 550,
            discountPrice: 300,
            duration: "3h 34m",
            level: "Beginner",
            coverImage: "images/courses/course-javascript.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 8,
            courseCode: "FE_WEB_CSS_111",
            courseName: "CSS: ultimate CSS course from beginner to advanced",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-css.jpg",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 9,
            courseCode: "FE_WEB_WORDPRESS_111",
            courseName: "Complete Wordpress themes & plugins",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Intermediate",
            coverImage: "images/courses/course-wordpress.jpg",
            teacherName: "Clevaio Simon",
            teacherPhoto: "images/teacher/clevaio_simon.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 10,
            courseCode: "FE_UIUX_COURSE_211",
            courseName: "Thinkful UX/UI Design Bootcamp",
            price: 950,
            discountPrice: 700,
            duration: "5h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-uiux.jpg",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: false,
            isTrending: false
        },
        {
            id: 11,
            courseCode: "FE_WEB_REACRJS_210",
            courseName: "Front-End Web Development with ReactJs",
            price: 1100,
            discountPrice: 850,
            duration: "6h 20m",
            level: "Advanced",
            coverImage: "images/courses/course-reactjs.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 12,
            courseCode: "FE_WEB_BOOTSTRAP_101",
            courseName: "Bootstrap 4 Crash Course | Website Build & Deploy",
            price: 750,
            discountPrice: 600,
            duration: "3h 15m",
            level: "Intermediate",
            coverImage: "images/courses/course-bootstrap.png",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 14,
            courseCode: "FE_WEB_RUBYONRAILS_310",
            courseName: "The Complete Ruby on Rails Developer Course",
            price: 2050,
            discountPrice: 1450,
            duration: "8h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-rubyonrails.png",
            teacherName: "Claire Robertson",
            teacherPhoto: "images/teacher/claire_robertson.jpg",
            isPopular: false,
            isTrending: true
        }
    ]
}
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
"use strict";
const gBASE_URL = "https://630890e4722029d9ddd245bc.mockapi.io/api/v1";
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
function onPageLoading() {
    callApiGetAllCourse();
}
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//Hàm gọi API lấy danh sách courses (all courses)
function callApiGetAllCourse() {
    var vAPI_URL = gBASE_URL + "/courses";
    $.ajax({
        url: vAPI_URL,
        type: "GET",
        success: function (paramSuccess) {
            console.log(paramSuccess)
            displayPopularCourse(paramSuccess);
            displayTrendingCourse(paramSuccess);
        },
        error: function (paramError) {
            console.log(paramError.responseText);
        }
    })
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//Hàm hiển thị các khóa học Popular
function displayPopularCourse(paramCourses) {
    for (let index = 0; index < paramCourses.length; index++) {
        if (paramCourses[index].isPopular === true) {
            $("#popular-courses-wrap").append(`
    <div class="col">
          <div class="card">
            <img src=${paramCourses[index].coverImage} class="card-img-top" alt="course-css" style="height:11.1rem">
            <div class="card-body">
              <h5 class="card-title card-title-css">${paramCourses[index].courseName}</h5>
              <div class="course-info">
                <i class="far fa-clock"></i> <span class="course-time"> ${paramCourses[index].duration}</span> <span
                  class="course-level">${paramCourses[index].level}</span>
              </div>
              <div class="courser-price-wrap mt-3 mb-2">
                <span class="course-new-price">$${paramCourses[index].discountPrice} </span><span class="course-old-price">$${paramCourses[index].price}</span>
              </div>
            </div>
            <div class="card-footer d-flex">
              <div class="teacher-info d-flex align-items-center justify-content-start gap-2">
                <img class="teacher-img" src=${paramCourses[index].teacherPhoto} alt=${paramCourses[index].teacherName}>
                <p class="teacher-name">${paramCourses[index].teacherName}</p>
              </div>
              <div class="bookmark-wrap">
                <i class="far fa-bookmark"></i>
              </div>
            </div>
          </div>
        </div>
    `)
        }
    }

}

//Hàm hiển thị các khóa học Trending
function displayTrendingCourse(paramCourses) {
    for (let index = 0; index < paramCourses.length; index++) {
        if (paramCourses[index].isTrending === true) {
            $("#trending-courses-wrap").append(`
    <div class="col">
          <div class="card">
            <img src=${paramCourses[index].coverImage} class="card-img-top" alt="course-css" style="height:11.1rem">
            <div class="card-body">
              <h5 class="card-title card-title-css">${paramCourses[index].courseName}</h5>
              <div class="course-info">
                <i class="far fa-clock"></i> <span class="course-time"> ${paramCourses[index].duration}</span> <span
                  class="course-level">${paramCourses[index].level}</span>
              </div>
              <div class="courser-price-wrap mt-3 mb-2">
                <span class="course-new-price">$${paramCourses[index].discountPrice} </span><span class="course-old-price">$${paramCourses[index].price}</span>
              </div>
            </div>
            <div class="card-footer d-flex">
              <div class="teacher-info d-flex align-items-center justify-content-start gap-2">
                <img class="teacher-img" src=${paramCourses[index].teacherPhoto} alt=${paramCourses[index].teacherName}>
                <p class="teacher-name">${paramCourses[index].teacherName}</p>
              </div>
              <div class="bookmark-wrap">
                <i class="far fa-bookmark"></i>
              </div>
            </div>
          </div>
        </div>
    `)
        }
    }
}
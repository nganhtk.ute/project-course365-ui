"use strict";

/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gBASE_URL = "https://624abe0dfd7e30c51c110ac6.mockapi.io/api/v1/";
let gCOURSE = ['STT', 'courseCode', 'courseName', 'duration', 'price', 'discountPrice', 'level', 'teacherName', 'id']
const gSTT_COL = 0;
const gCOURSE_CODE_COL = 1;
const gCOURSE_NAME_COL = 2;
const gDURATION_COL = 3;
const gPRICE_COL = 4;
const gDISCOUNT_PRICE_COL = 5;
const gLEVEL_COL = 6;
const gTEACHER_NAME_COL = 7;
const gID_COL = 8;
let gCOURSE_LIST = [];
let gCOURSE_ID;
// let gOrderId = '';
let gCourseTable = $('#table-course-list').DataTable({
    columns: [
        {
            data: gCOURSE[gSTT_COL],
            title: '#',
        },
        {
            data: gCOURSE[gCOURSE_CODE_COL],
            title: 'Course Code',
        },
        {
            data: gCOURSE[gCOURSE_NAME_COL],
            title: 'Course Name',
        },
        {
            data: gCOURSE[gDURATION_COL],
            title: 'Duration',
        },
        {
            data: gCOURSE[gPRICE_COL],
            title: 'Price',
        },
        {
            data: gCOURSE[gDISCOUNT_PRICE_COL],
            title: 'Discount Price',
        },
        {
            data: gCOURSE[gLEVEL_COL],
            title: 'Level',
        },
        {
            data: gCOURSE[gTEACHER_NAME_COL],
            title: 'Teacher Name',
        },
        {
            data: gCOURSE[gID_COL],
            title: 'Action',
        },
    ],
    columnDefs: [
        {
            targets: gSTT_COL,
            class: "text-center",
            render: function (data, type, row, meta) {
                return meta.row + 1;
            }
        },
        {
            targets: gCOURSE_CODE_COL,
            class: "text-left",
        },
        {
            targets: gCOURSE_NAME_COL,
            class: "text-left",
            width: "50%",
        },
        {
            targets: gDISCOUNT_PRICE_COL,
            class: "text-center",
        },
        {
            targets: gID_COL,
            render: function (id, type) {
                if (type === "display") {
                    return `
                    <div class="d-flex justify-content-center">
                        <button data-id=${id} class="btn edit-course text-info"><i class="fa-solid fa-pen" data-toggle="tooltip" data-placement="bottom" title="Edit"></i></button>
                        <button data-id=${id} class="btn delete-course text-danger"><i class="fa-regular fa-circle-xmark" data-toggle="tooltip" data-placement="bottom" title="Delete"></i></button>       
                    </div>
                `;
                }
                return id;
            }
        },

    ]
})

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
// Hàm xử lý sự kiện tải trang
function onPageLoading() {
    $(".overlay").show();
    callApiGetCourseList();
    //Gán sự kiện cho nút create 
    $(".btn-create-course").click(function () {
        clearDataModal()
        $(".modal-create").modal("show");
        $("#btn-create").css("display", "block");
        $("#btn-edit").css("display", "none");
        $('#input-create-course-code').trigger('focus');
    })
}

//Gán sự kiện cho nút sửa
$('#table-course-list').on('click', '.edit-course', function () {
    gCOURSE_ID = this.dataset.id;

    //Gọi API để lấy khóa học theo ID
    callApiGetCourseById();
})

//Gán sự kiện cho nút delete
$('#table-course-list').on('click', '.delete-course', function () {
    gCOURSE_ID = this.dataset.id;
    $(".modal-delete").modal("show");
})

//Gán sự kiện cho nút confirm (modal)
$("#btn-confirm").click(function () {
    //Gọi API để xóa khóa học theo ID
    callApiDeleteCourseById();
})

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// Hàm gọi api lấy danh sách khóa học
function callApiGetCourseList() {
    var vAPI_URL = gBASE_URL + "/courses";
    $.ajax({
        url: vAPI_URL,
        type: 'GET',
        success: function (paramSuccess) {
            gCOURSE_LIST = paramSuccess;
            displayDataToTable();
            $(".overlay").hide();
        }
    })

}
// Hàm gọi API lấy thông tin khóa học theo ID
function callApiGetCourseById() {
    const vAPI_URL = gBASE_URL + "/courses/" + gCOURSE_ID;
    $.ajax({
        url: vAPI_URL,
        type: "GET",
        success: function (paramSuccess) {
            console.log(paramSuccess)
            $(".modal-create").modal("show");
            $("#btn-create").css("display", "none");
            $("#btn-edit").css("display", "block");
            $('#input-create-course-code').trigger('focus');
            displayDataToModal(paramSuccess);
        },
        error: function (paramError) {
            console.log(paramError.responseText)
        }
    })
}

// Hàm sửa thông tin khóa học theo ID
function editCourse(paramButton) {
    //Đọc dữ liệu
    var vCourseObjRequest = {
        courseCode: "",
        courseName: "",
        price: 0,
        discountPrice: 0,
        duration: "",
        level: "",
        coverImage: "",
        teacherName: "",
        teacherPhoto: "",
        isPopular: false,
        isTrending: false
    };
    console.log(vCourseObjRequest);
    readData(vCourseObjRequest);
    let dataIsValidated = checkData(vCourseObjRequest);
    if (dataIsValidated) {
        callApiEditCourse(vCourseObjRequest);
    }
}

// Hàm gọi API xóa khóa học theo ID 
function callApiDeleteCourseById() {
    const vAPI_URL = gBASE_URL + "/courses/";
    $.ajax({
        url: vAPI_URL + gCOURSE_ID,
        type: "DELETE",
        success: function (paramSuccess) {
            $(".modal-delete").modal("hide");
            alert("Xóa khóa học thành công");
            window.location.reload();
        },
        error: function (paramError) {
            alert(paramError.responseText);
        }
    })
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// Hàm tạo mới course
function createNewCourse() {
    var vCourseObjRequest = {
        courseCode: "",
        courseName: "",
        price: 0,
        discountPrice: 0,
        duration: "",
        level: "",
        coverImage: "",
        teacherName: "",
        teacherPhoto: "",
        isPopular: false,
        isTrending: false
    };
    console.log(vCourseObjRequest);
    //Đọc dữ liệu
    readData(vCourseObjRequest);
    // Kiểm tra dữ liệu

    let isDataValidated = checkData(vCourseObjRequest);
    if (isDataValidated) {
        //Gọi api tạo mới khóa học
        callApiCreateCourse(vCourseObjRequest);
    }
}

//Hàm hiển thị Course Image
function chooseCourseImage(paramInput) {
    // const myFile = paramInput.files[0].name;
    const myFile = paramInput.files[0];
    let vCourseSize = paramInput.files[0].size;
    console.log(vCourseSize);
    console.log(myFile);
    // $(".choose-course-image").html("");
    // $("#course-image").attr("src", "https://media-devcamp.netlify.app/course365/courses/" + myFile);
    // $("#course-image").css("display", "block")

    const vFile = Math.round((vCourseSize / 1024));

    if (vFile < 40) {
        var reader = new FileReader();
        reader.onload = function (event) {
            $(".choose-course-image").html("");
            $("#course-image").attr("src", event.target.result);
            $("#course-image").css("display", "block");
        };
        reader.readAsDataURL(myFile);
    } else {
        $("#label-course-image ~ .invalid-feedback-css").text("File too Big");
        $(".invalid-feedback-css").removeAttr("hidden");
    }
}

//Hàm hiển thị Teacher Image
function chooseTeacherImage(paramInput) {
    // const myFile = paramInput.files[0].name;
    const myFile = paramInput.files[0];
    let vTeacherSize = paramInput.files[0].size;
    console.log(vTeacherSize);
    console.log(myFile)

    const vFile = Math.round((vTeacherSize / 1024));
    // The size of the file.
    if (vFile < 40) {
        var reader = new FileReader();
        reader.onload = function (event) {
            $(".choose-teacher-image").html("");
            $("#teacher-image").attr("src", event.target.result);
            $("#teacher-image").css("display", "block");
        };
        reader.readAsDataURL(myFile);
    } else {
        $("#label-teacher-image ~ .invalid-feedback-css").text("File too Big");
        $(".invalid-feedback-css").removeAttr("hidden");
    }
    // if (vFile >= 40) {
    //     $("#label-teacher-image ~ .invalid-feedback-css").text("File too Big, please select a file less than 4mb");

    // } else if (vFile < 20) {
    //     $("#label-teacher-image ~ .invalid-feedback-css").text("File too small, please select a file greater than 2mb");
    // } else {

    // }

    // $(".choose-teacher-image").html("");
    // $("#teacher-image").attr("src", "https://media-devcamp.netlify.app/course365/teacher/" + myFile);
    // $("#teacher-image").css("display", "block");



}

// Hàm đọc dữ liệu trên modal
function readData(paramCourseObj) {

    paramCourseObj.courseCode = $.trim($("#input-create-course-code").val());

    paramCourseObj.courseName = $.trim($("#input-create-course-name").val());

    paramCourseObj.price = Number(Math.round($.trim($("#input-create-price").val())));

    paramCourseObj.discountPrice = Number(Math.round($.trim($("#input-create-discount-price").val())));

    paramCourseObj.duration = $.trim($("#input-create-duration").val());

    paramCourseObj.level = $("#select-create-level").val();

    paramCourseObj.coverImage = $("#course-image").attr("src");

    paramCourseObj.teacherName = $.trim($("#input-create-teacher-name").val());

    paramCourseObj.teacherPhoto = $("#teacher-image").attr("src");

    paramCourseObj.isPopular = $("input[name='popular']:checked").val();

    paramCourseObj.isTrending = $("input[name='trending']:checked").val();


}

// Hàm tải đơn hàng vào table
function displayDataToTable() {
    gCourseTable.clear();
    gCourseTable.rows.add(gCOURSE_LIST);
    gCourseTable.draw();

    $('#table-course-list thead').addClass("text-info");
    $('#table-course-list thead').addClass("text-center");
    $('#table-course-list thead tr th').css("vertical-align", "middle");

    //Hàm chạy tooltip
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
    // $(".overlay").hide();
}

//Hàm xử lý hiển thị data lên modal
function displayDataToModal(paramObj) {
    $("#input-create-course-code").val(paramObj.courseCode);

    $("#input-create-course-name").val(paramObj.courseName);

    $("#input-create-price").val(paramObj.price);

    $("#input-create-discount-price").val(paramObj.discountPrice);

    $("#input-create-duration").val(paramObj.duration)

    $("#select-create-level").val(paramObj.level);

    $("#course-image").attr("src", paramObj.coverImage);
    $(".choose-course-image").html("");
    $("#course-image").css("display", "block")


    $("#input-create-teacher-name").val(paramObj.teacherName)

    $("#teacher-image").attr("src", paramObj.teacherPhoto);
    $(".choose-teacher-image").html("");
    $("#teacher-image").css("display", "block");


    if (paramObj.isPopular === "true" || paramObj.isPopular === true) {
        $("input[name='popular']")[0].checked = true;
    } else {
        $("input[name='popular']")[1].checked = true;
    }

    if (paramObj.isTrending === "true" || paramObj.isTrending === true) {
        $("input[name='trending']")[0].checked = true;
    } else {
        $("input[name='trending']")[1].checked = true;
    }

}
// Hàm gọi Api tạo khóa học mới
function callApiCreateCourse(paramCourseObj) {

    const vAPI_URL = gBASE_URL + "/courses";

    $.ajax({
        url: vAPI_URL,
        type: "POST",
        dataType: "JSON",
        data: JSON.stringify(paramCourseObj),
        contentType: "application/json",
        success: function (result) {
            alert("Tạo khóa học thành công")
            $(".modal-create").modal("hide");
            window.location.reload();
        },
        error: function (paramError) {
            console.log(paramError.responseText);
        }

    })
}

// Hàm gọi Api sửa khóa học theo id
function callApiEditCourse(paramCourseObj) {

    const vAPI_URL = gBASE_URL + "/courses/";

    $.ajax({
        url: vAPI_URL + gCOURSE_ID,
        type: "PUT",
        dataType: "JSON",
        data: JSON.stringify(paramCourseObj),
        contentType: "application/json",
        success: function (result) {
            alert("Sửa khóa học thành công")
            $(".modal-create").modal("hide");
            window.location.reload();
        },
        error: function (paramError) {
            console.log(paramError.responseText);
        }

    })
}

// Hàm kiểm tra dữ liệu 
function checkData(paramCourseObj) {

    $(".form-control").removeClass("is-invalid");
    let isValid = true;
    //Mã khóa học: Text, chứa ít nhất 10 ký tự
    if (paramCourseObj.courseCode === "") {
        $("#input-create-course-code").addClass("is-invalid");
        $("#input-create-course-code ~.invalid-feedback").text("Vui lòng nhập mã khóa học");
        isValid = false;
    }
    else if (paramCourseObj.courseCode.length < 10) {
        $("#input-create-course-code").addClass("is-invalid");
        $("#input-create-course-code ~.invalid-feedback").text("Mã khóa học phải từ 10 kí tự trở lên");
        isValid = false;
    }

    //Tên khóa học: Text, chứa ít nhất 20 ký tự
    if (paramCourseObj.courseName === "") {
        $("#input-create-course-name").addClass("is-invalid");
        $("#input-create-course-name ~ .invalid-feedback").text("Vui lòng nhập tên khóa học");
        isValid = false;
    } else if (paramCourseObj.courseName.length < 20) {
        $("#input-create-course-name").addClass("is-invalid");
        $("#input-create-course-name ~ .invalid-feedback").text("Tên khóa học phải từ 20 kí tự trở lên");
        isValid = false;
    }

    //Giá: Là số nguyên, > 0
    if (paramCourseObj.price === "") {
        $("#input-create-price").addClass("is-invalid");
        $("#price-invalid-feedback").text("Vui lòng nhập giá khóa học");
        $("#price-invalid-feedback").css("display", "block");
        isValid = false;
    } else if (paramCourseObj.price <= 0) {
        $("#input-create-price").addClass("is-invalid");
        $("#price-invalid-feedback").text("Giá khóa học phải lớn hơn 0");
        $("#price-invalid-feedback").css("display", "block");
        isValid = false;
    }

    //Giá giảm : Là số nguyên, >= 0 và phải <= price
    if (paramCourseObj.discountPrice)
        if (paramCourseObj.discountPrice < 0 || paramCourseObj.discountPrice > paramCourseObj.price) {
            $("#input-create-discount-price").addClass("is-invalid");
            $("#discount-price-invalid-feedback").text("Giá giảm phải lớn hơn 0 hoặc bằng 0 và bé hơn giá bán");
            $("#discount-price-invalid-feedback").css("display", "block");
            isValid = false;
        }

    // Cover Image: text 
    if (paramCourseObj.coverImage === "") {
        $("#label-course-image ~ .invalid-feedback-css").text("Vui lòng chọn hình khóa học");
        $(".invalid-feedback-css").removeAttr("hidden");
        isValid = false;
    }

    // Teacher Photo: text
    if (paramCourseObj.teacherPhoto === "") {
        $("#label-teacher-image ~ .invalid-feedback-css").text("Vui lòng chọn hình giáo viên");
        $(".invalid-feedback-css").removeAttr("hidden");

        isValid = false;
    }

    // Level: text
    if (paramCourseObj.level === "") {
        $("#select-create-level").addClass("is-invalid");
        $("#level-invalid-feedback").text("Vui lòng chọn level");
        $("#level-invalid-feedback").css("display", "block");
        isValid = false;
    }

    // Duration: text
    if (paramCourseObj.duration === "") {
        $("#input-create-duration").addClass("is-invalid");
        $("#duration-invalid-feedback").text("Vui lòng nhập thời lượng");
        $("#duration-invalid-feedback").css("display", "block");
        isValid = false;
    }

    // isPopular: boolean
    if ($("input[name='popular']:checked").length === 0) {
        $(".question ~ .invalid-feedback-css").text("Vui lòng chọn true or false");
        $(".question ~ .invalid-feedback-css").removeAttr("hidden")
    }

    // isTrending: boolean
    if ($("input[name='trending']:checked").length === 0) {
        $(".question ~ .invalid-feedback-css").text("Vui lòng chọn true or false");
        $(".question ~ .invalid-feedback-css").removeAttr("hidden")
    }

    //Teacher name: Text
    if (paramCourseObj.teacherName === "") {
        $("#input-create-teacher-name").addClass("is-invalid");
        $("#input-create-teacher-name ~.invalid-feedback").text("Vui lòng nhập tên giáo viên");
        isValid = false;
    }
    return isValid;
}

// API Post Order
function updateOrder(paramUpdateOrder) {
    var vVoucherCode = $('#input-edit-discount').val();
    console.log(vVoucherCode);
    $.ajax({
        url: gBASE_URL + "/vouchers" + "?" + 'voucherCode=' + vVoucherCode,
        type: "GET",
        success: function (paramSuccess) {
            console.log(paramSuccess);
            paramUpdateOrder.voucherId = paramSuccess[0].id;
            console.log(paramUpdateOrder);
            $.ajax({
                url: gBASE_URL + "/orders/" + gOrderId,
                type: "PUT",
                contentType: "application/json",
                data: JSON.stringify(paramUpdateOrder),
                success: function (paramSuccess) {
                    // console.log(paramSuccess);
                    alert('Update đơn hàng thành công')
                    $('.modal-edit').html('');
                    $('.modal-edit').modal('hide');
                    location.reload();
                }
            })
        },
        error: function (paramError) {
            console.assert(paramError.responseText);
        }
    })
}

// API xóa 1 Order theo Id
function callApiDeleteOrder(paramOrderId) {
    console.log(paramOrderId)
    $.ajax({
        url: gBASE_URL + "/orders/" + paramOrderId,
        type: "DELETE",
        success: function (paramSuccess) {
            console.log(paramSuccess);
            alert('Xóa đơn hàng thành công')
            location.reload();
        }
    })
}

// Hàm clear hết data trên modal
function clearDataModal() {
    $("#input-create-course-code").val("");

    $("#input-create-course-name").val("");

    $("#input-create-price").val("");

    $("#input-create-discount-price").val("");

    $("#input-create-duration").val("")

    $("#select-create-level").val("");

    $("#course-image").attr("src", "");
    $("#course-image").css("display", "none");
    $(".choose-course-image").html("Choose course image");

    $("#input-create-teacher-name").val("")

    $("#teacher-image").attr("src", "");
    $("#teacher-image").css("display", "none");
    $(".choose-teacher-image").html("Choose teacher image");

    $("input[name='popular']")[0].checked = false;
    $("input[name='popular']")[1].checked = false;

    $("input[name='trending']")[0].checked = false;
    $("input[name='trending']")[1].checked = false;

}